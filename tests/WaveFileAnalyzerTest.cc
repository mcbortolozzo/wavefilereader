#include "../src/wave/WaveFileAnalyzer.cc"
#include <cstdlib>
#include <vector>
#include <fstream>
#include <sstream>

#define EXPECTED_CHANNEL_PATH "tests/expected_channels.csv"
#define EXPECTED_HANN_PATH "tests/expected_hann.csv"

std::vector<std::vector<double>> ReadExpectedChannels()
{
    std::vector<std::vector<double>> channels;
    std::vector<double> channel1;
    std::vector<double> channel2;

    std::ifstream file(EXPECTED_CHANNEL_PATH);
    double c1, c2;
    char s;
    while(file >> c1 >> s >> c2)
    {
        channel1.push_back(c1);
        channel2.push_back(c2);
    }

    channels.push_back(channel1);
    channels.push_back(channel2);
    return channels;
}

std::vector<std::vector<double>*> ReadExpectedWindowsBinary()
{
    std::vector<std::vector<double>*> windows;
    
    std::ifstream file("tests/xbinary.bin");

    uint16_t row_count;
    file.read(reinterpret_cast<char *>(&row_count), sizeof(row_count));

    uint16_t column_count;
    file.read(reinterpret_cast<char *>(&column_count), sizeof(column_count));

    std::cout << "row count " << row_count << std::endl;
    std::cout << "column count " << column_count << std::endl;

    for(int i = 0; i < column_count; i++)
    {
        int count = 0;
        windows.push_back(new std::vector<double>);
        for(int j = 0; j < row_count; j++)
        {
            double value;
            file.read(reinterpret_cast<char *>(&value), sizeof(value));
            windows[i]->push_back(value);
            if(value != 0) count ++;
        }
    }

    return windows;
}

std::vector<std::vector<double>*> ReadExpectedFFTBinary()
{
    std::vector<std::vector<double>*> windows;
    
    std::ifstream file("tests/fftbinary.bin");

    uint16_t row_count;
    file.read(reinterpret_cast<char *>(&row_count), sizeof(row_count));

    uint16_t column_count;
    file.read(reinterpret_cast<char *>(&column_count), sizeof(column_count));

    std::cout << "row count " << row_count << std::endl;
    std::cout << "column count " << column_count << std::endl;

    for(int i = 0; i < column_count; i++)
    {
        int count = 0;
        windows.push_back(new std::vector<double>);
        for(int j = 0; j < row_count; j++)
        {
            double value;
            file.read(reinterpret_cast<char *>(&value), sizeof(value));
            windows[i]->push_back(value);
            if(value != 0) count ++;
        }
    }

    return windows;
}

std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str)
{
    std::vector<std::string>   result;
    std::string                line;
    std::getline(str,line);

    std::stringstream          lineStream(line);
    std::string                cell;

    while(std::getline(lineStream,cell, ','))
    {
        result.push_back(cell);
    }
    // This checks for a trailing comma with no data after it.
    if (!lineStream && cell.empty())
    {
        // If there was a trailing comma then add an empty element.
        result.push_back("");
    }
    return result;
}

std::vector<std::vector<double>*> ReadExpectedWindows()
{
    std::vector<std::vector<double>*> windows;
    bool initialized = false;
    std::ifstream file(EXPECTED_HANN_PATH);
    int index = 0;

    std::cout << "Reading " << EXPECTED_HANN_PATH << std::endl;

    while(!file.eof())
    {
        std::vector<std::string> next_line = getNextLineAndSplitIntoTokens(file);
        if(next_line.size() < 40)
            continue;
        for(int i = 0; i < next_line.size(); i++)
        {
            if(!initialized)
                windows.push_back(new std::vector<double>());
            
            windows[i]->push_back(std::stod(next_line[i]));
        }    

        initialized = true;
    }

    return windows;
}

void TestFileBufferRead()
{
    WaveFileReader<float> *reader = new WaveFileReader<float>("audio_test.wav");
    float* file_buffer;
    int file_size = reader->ReadBuffer(&file_buffer);
    free(reader);

    std::vector<std::vector<double>> expected_channels = ReadExpectedChannels();
    int count = 0;    
    int channel_length = expected_channels[0].size();

    for(int i = 0; i < channel_length; i++)
    {
        count++;
        if(file_buffer[2*i] != expected_channels[0][i])
        {
            std::cout << "FAILED TO READ BUFFER, channel " << 0
                << " at position " << i
                << " read " << file_buffer[2*i] 
                << " expected " << expected_channels[0][i] << std::endl;
            exit(-1);
        }
        count++;
        if(file_buffer[2*i + 1] != expected_channels[1][i])
        {
            std::cout << "FAILED TO READ BUFFER, channel " << 1
                << " at position " << i
                << " read " << file_buffer[2*i + 1] 
                << " expected " << expected_channels[1][i] << std::endl;
            exit(-1);
        }
    }
    std::cout << "compared " << count << " items" << std::endl;
    std::cout << "----Passed File Buffer Test----" << std::endl;    
    free(file_buffer);
}

void TestChannelSplit()
{
    WaveFileReader<float> *reader = new WaveFileReader<float>("audio_test.wav");
    WaveFile<float> *file = reader->ReadFile();
    free(reader);

    std::vector<std::vector<double>> expected_channels = ReadExpectedChannels();
    int count = 0;    
    int channel_length = expected_channels[0].size();


    for(int i = 0; i < expected_channels.size(); i++)
    {
        for(int j = 0; j < channel_length; j++)
        {
            count++;
            if((*file)[i][j] != expected_channels[i][j])
            {
                std::cout << "FAILED TO READ BUFFER, channel " << i
                    << " at position " << j
                    << " read " << (*file)[i][j]
                    << " expected " << expected_channels[i][j] << std::endl;
                exit(-1);
            }
        }
    }
    std::cout << "compared " << count << " items" << std::endl;
    std::cout << "----Passed Channels Test----" << std::endl;
    free(file);
}

void TestWindowedRead()
{
    std::vector<std::vector<double>*> expected_windows = ReadExpectedWindowsBinary();
    WaveFileAnalyzer<float> *analyzer = new HannWaveFileAnalyzer<float>("audio_test.wav", 8192, 4096, 8192);
    float** windows = analyzer->Analyze();
    free(analyzer);

    int count = 0;

    for(int i = 0; i < expected_windows.size(); i++)
    {
        for(int j = 0; j < expected_windows[i]->size(); j++)
        {
            count++;
            if(std::abs(windows[i][j] - (*expected_windows[i])[j]) > 0.0001)
            {       
                std::cout << "FAILED TO READ BUFFER, window " << i
                    << " at position " << j
                    << " read " << windows[i][j]
                    << " expected " << (*expected_windows[i])[j] << std::endl;

                for(int k = j; k < expected_windows[i]->size(); k++)
                {
                    if(windows[i][j] == (*expected_windows[i])[j])
                    {
                        std::cout << "found at " << k << std::endl;
                        exit(-1);
                    }
                }
                std::cout << "not found in window" << std::endl;

                exit(-1);
            }
        }
    }
    std::cout << "compared " << count << " items" << std::endl;
    std::cout << "----Passed Hann window Test----" << std::endl;
}

void TestFFT()
{
    std::vector<std::vector<double>*> expected_fft = ReadExpectedFFTBinary();
    WaveFileAnalyzer<float> *analyzer = new HannWaveFileAnalyzer<float>("audio_test.wav", 8192, 4096, 8192);
    float** windows = analyzer->Analyze();
    analyzer->RunFFT(windows);
    free(analyzer);

    int count = 0;

    for(int i = 0; i < expected_fft.size(); i++)
    {
        for(int j = 0; j < expected_fft[i]->size(); j++)
        {
            count++;
            if(std::abs(windows[i][j] - (*expected_fft[i])[j]) > 0.0001)
            {
                std::cout << windows[i][j] << ";" << windows[i][j+1] << ";" << windows[i][j+2] << ";" << windows[i][j+3] << ";" << windows[i][j+4] <<std::endl;

                std::cout << "FAILED TO READ BUFFER, window " << i
                    << " at position " << j
                    << " read " << windows[i][j]
                    << " expected " << (*expected_fft[i])[j] << std::endl;

                for(int k = j; k < expected_fft[i]->size(); k++)
                {
                    if(windows[i][j] == (*expected_fft[i])[j])
                    {
                        std::cout << "found at " << k << std::endl;
                        exit(-1);
                    }
                }
                std::cout << "not found in window" << std::endl;

                exit(-1);
            }
        }
    }
    std::cout << "compared " << count << " items" << std::endl;
    std::cout << "----Passed FFT Test----" << std::endl;
}

int main()
{
    TestFileBufferRead();
    TestChannelSplit();
    TestWindowedRead();
    TestFFT();
}