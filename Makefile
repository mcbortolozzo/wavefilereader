CXX = g++

ccsrc = $(wildcard src/*.cc) \
       $(wildcard src/wave/*.cc)

ccsrctest = $(wildcard tests/*.cc)

obj = $(ccsrc:.cc=.o)
objtest = $(ccsrctest:.cc=.o)

LDFLAGS = -lsndfile -lfftw3

wavefileopener: $(obj) Main.cc
	$(CXX) -o $@ $^ $(LDFLAGS)

test: $(obj) $(objtest)
	$(CXX) -o $@ $^ $(LDFLAGS) -o wavefileopenertest

clean:
	rm -f $(obj) wavefileopener