#include <unistd.h>

#include "src/wave/WaveFileAnalyzer.cc"

void output_windows(const char* filename, double** windows, int window_count, int window_length)
{
	ofstream outfile;
	outfile.open(filename);
	for(int i = 0; i < window_count; i ++)
	{
		outfile << ";W" << i;
	}

	outfile << std::endl;

	for(int i = 0; i < window_length; i ++)
	{
		outfile << i;
		for(int j = 0; j < window_count; j++)
		{	
			outfile << ";" << windows[j][i];
		}
		outfile << std::endl;
	}
	outfile.close();
} 

int main(int argc, char *argv[])
{
	int opt;
	int window_size = WINDOW_SIZE, bucket_size = WINDOW_SIZE, step_size = STEP_SIZE;
	char* file_name = new char[256];
	char* outfile = new char[256];
	while((opt = getopt(argc, argv, "f:w:s:b:o:")) != -1)
	{
		switch(opt)
		{
			case 'f': memcpy(file_name, optarg, sizeof(char)*strlen(optarg)); break;
			case 'w': window_size = atoi(optarg); break;
			case 's': step_size = atoi(optarg); break;
			case 'b': bucket_size = atoi(optarg); break;
			case 'o': memcpy(outfile, optarg, sizeof(char)*strlen(optarg));break;
			default: printf("usage: wavefileopener -f filename -w window_size -s step_size -b bucket_size -o output_file\n"); exit(-1);
		}
	}

	std::cout << "Running Analysis" << std::endl
		<< "File: " << file_name << std::endl
		<< "Window Size: " << window_size << std::endl
		<< "Step Size: " << step_size << std::endl
		<< "Bucket Size: " << bucket_size << std::endl << std::endl;
        
    HannWaveFileAnalyzer<double> *analyzer = new HannWaveFileAnalyzer<double>(file_name, window_size, step_size, bucket_size);
    double** windows = analyzer->Analyze();
    analyzer->RunFFT(windows);
    output_windows(outfile, windows, analyzer->GetWindowCount(), bucket_size);
}
