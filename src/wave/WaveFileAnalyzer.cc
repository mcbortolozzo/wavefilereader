#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <stdio.h>
#include <sndfile.h>
#include <math.h>
#include <fftw3.h>

#include "WaveFileReader.hpp"
#include "WaveFile.hpp"
#include "WaveWindowIterator.hpp"

using namespace std;

#define PI 3.14159265

template<typename value_type>
class WaveFileAnalyzer
{
    #define READ_BUFFER_SIZE 65536
    #define WINDOW_SIZE 8192
    #define STEP_SIZE 4096

    private:
        WaveFile<value_type> *wave_file_;
           
        virtual value_type* window_function(value_type* window) = 0;                

        value_type** read_windowed(int channelNb)
        {            
            std::cout << "Reading channel: " << channelNb
                << ", with window size: "<< WindowSize
                << " and step: " << StepSize <<std::endl;

            WaveFileChannel<value_type> channel = (*wave_file_)[channelNb];
            value_type *padded_buffer = new value_type[2*(WindowSize - 1) + channel.Length()];
            for(int i = 0; i < WindowSize - 1; i ++)
            {
                padded_buffer[i] = 0;
                padded_buffer[channel.Length() - 1 - i] = 0;
            }
            memcpy(padded_buffer + WindowSize - 1, channel.Buffer(), channel.Length() * sizeof(value_type));            

            int window_count = GetWindowCount();
            value_type **windowed_channel = new value_type*[window_count];

            for(int i = 0; i < window_count; i++)
            {
                windowed_channel[i] = window_function(padded_buffer + (WindowSize - 1) + i*StepSize); 
            }            

            return windowed_channel;
        }        

    public:
        int WindowSize;
        int StepSize;
        int BucketSize;

        inline int GetWindowCount()
        {
            std::cout << "step: " << StepSize << " chanel: " << wave_file_->ChannelLength() << std::endl;
            return (int) (wave_file_->ChannelLength()/StepSize + ((wave_file_->ChannelLength() % StepSize) != 0));
        }

        value_type** Analyze()
        {            
            std::cout << "Starting Analysis" << std::endl
                << "Sample Rate: " << wave_file_->SampleRate() << std::endl
                << "Channels: " << wave_file_->ChannelCount() << std::endl
                << "Format: WAV" << std::endl;  

            std::cout << "Total file size: " << wave_file_->Size() << std::endl
                << "Length by channel: " <<  wave_file_->Size() / wave_file_->ChannelCount() << std::endl;     

            std::cout << "Reading channel with windowing " << std::endl
                << "Window size: " << WindowSize << ", Step size: " << StepSize << std::endl;
            
            int window_count = GetWindowCount();
            value_type **windowed_read = read_windowed(0);

            std::cout << "# windows: " << window_count << std::endl;

            std::cout << "Analysis Finished" << std::endl;

            return windowed_read;
        }

        void RunFFT(value_type** windows)
        {
            fftw_complex *in, *out;
            fftw_plan p;
            in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * BucketSize);
            out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * BucketSize);
            p = fftw_plan_dft_1d(BucketSize, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

            int window_count = GetWindowCount();

            for(int i = 0; i < window_count; i++)
            {
                for(int j = 0; j< WindowSize; j++)
                {
                    in[j][0] = windows[i][j];
                }

                fftw_execute(p);

                free(windows[i]);
                windows[i] = new value_type[BucketSize];

                for(int j = 0; j < BucketSize; j++)
                {                    
                    windows[i][j] = sqrt(pow(out[j][0],2) + pow(out[j][1],2));
                }
            }

            fftw_destroy_plan(p);
            fftw_free(in);
            fftw_free(out);
        }

        WaveFileAnalyzer(const char* filename, int window_size = WINDOW_SIZE, int step_size = STEP_SIZE, int bucket_size = WINDOW_SIZE)
        {
            WindowSize = window_size;
            StepSize = step_size;
            BucketSize = bucket_size;

            WaveFileReader<value_type> *reader = new WaveFileReader<value_type>(filename);
            wave_file_ = reader->ReadFile();
            free(reader);
        }

};

template<typename value_type>
class HannWaveFileAnalyzer : public WaveFileAnalyzer<value_type>
{
    private:
        value_type* window_function(value_type* window)
        {   
            value_type *buffer = new value_type[this->WindowSize];
            for(int i = 0; i < this->WindowSize; i++)
            {
                buffer[i] = window[i] * 0.5 * (1 - cos(2*PI*i/(this->WindowSize - 1)));
            }    
            return buffer;
        }

    public:
        HannWaveFileAnalyzer(const char* filename, int window_size = WINDOW_SIZE, int step_size = STEP_SIZE, int bucket_size = WINDOW_SIZE)
            : WaveFileAnalyzer<value_type>(filename, window_size, step_size, bucket_size)
        { }
};