#pragma once

#include <sndfile.h>
#include <iostream>
#include "WaveFile.hpp"
#include "WaveFileChannel.hpp"

template<typename value_type>
class WaveFileReader
{
    public:
        WaveFileReader(const char *path);
        WaveFile<value_type>* ReadFile();
        int ReadBuffer(value_type** file_buffer);

    private:
        const char *file_path_;

        SNDFILE *file_handle_;
        SF_INFO info_;

        inline int read_buffer(value_type *buffer, int read_size);
};

template<typename value_type>
WaveFileReader<value_type>::WaveFileReader(const char *path)
{
    file_path_ = path;

    std::cout << "Opening file " << file_path_ << std::endl;
    file_handle_ = sf_open(file_path_, SFM_READ, &info_);
    if(int err = sf_error(file_handle_) != 0)
    {
        std::cout << "ERROR: " << err << std::endl;
        exit(-1);
    }   

    if(info_.format && SF_FORMAT_WAV == 0)
    {
        std::cout << "INVALID FORMAT: ONLY WAV FILES ALLOWED" << std::endl;
        exit(-1);
    }
}

template<typename value_type>
WaveFile<value_type>* WaveFileReader<value_type>::ReadFile()
{
    value_type* file_buffer;
    int file_size = this->ReadBuffer(&file_buffer);

    return new WaveFile<value_type>(info_, file_size, file_buffer);
}

template<typename value_type>
int WaveFileReader<value_type>::read_buffer(value_type* buffer, int read_size)
{
    exit(-1);
}

template<>
inline int WaveFileReader<float>::read_buffer(float* buffer, int read_size)
{
    return sf_read_float(file_handle_, buffer, read_size);
}

template<>
inline int WaveFileReader<double>::read_buffer(double* buffer, int read_size)
{
    return sf_read_double(file_handle_, buffer, read_size);
}

template<typename value_type>
int WaveFileReader<value_type>::ReadBuffer(value_type **file_buffer)
{
    int buffer_size = info_.channels * info_.frames;
    *file_buffer = new value_type[buffer_size];

    int read = read_buffer(*file_buffer, buffer_size);
    // TODO validate read size

    return read;
}