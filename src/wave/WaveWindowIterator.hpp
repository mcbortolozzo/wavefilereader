#pragma once

#include <iostream>

template<typename value_type>
class WaveWindowIterator {
    public:
        WaveWindowIterator(value_type* buffer, int buffer_size, int starting_index, int before_padding_size, int after_padding_size);
        WaveWindowIterator& operator++() { index_++; return *this; }
        bool operator==(WaveWindowIterator other) const {return (index_ == other.index_) && (buffer_ == other.buffer_);}
        bool operator!=(WaveWindowIterator other) const {return !(*this == other);}
        value_type operator*();
        
        // iterator traits
        using difference_type = long;
        using pointer = const long*;
        using reference = const long&;
        using iterator_category = std::forward_iterator_tag;

    private:
        long index_ = 0;
        value_type *buffer_;
        int before_padding_size_;
        int after_padding_size_;
        int buffer_size_;
};

#include "WaveWindowIterator.hpp"

// Maybe add window size
template<typename value_type>
WaveWindowIterator<value_type>::WaveWindowIterator(value_type* buffer, int buffer_size, int starting_index, int before_padding_size, int after_padding_size)
{
    buffer_size_ = buffer_size;
    before_padding_size_ = before_padding_size;
    after_padding_size_ = after_padding_size;
    buffer_ = buffer;
    index_ = starting_index;
}

template<typename value_type>
value_type WaveWindowIterator<value_type>::operator*()
{
    if(index_ < -1*before_padding_size_ || index_ >= buffer_size_ + after_padding_size_)
    {
        std::cout << "out of bounds window iter, index: " << index_ << " before padding: " << before_padding_size_ << " after padding: " << after_padding_size_ << std::endl;
        exit(-1);
    }
    
    if(index_ < 0 || index_ >= buffer_size_)
        return 0;
    else
        return buffer_[index_];
}