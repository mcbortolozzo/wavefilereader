#pragma once

#include <sndfile.h>
#include <iostream>

#include "WaveFileChannel.hpp"

template<typename value_type>
class WaveFile
{
    public:
        WaveFile(SF_INFO info, int wave_size, value_type* file_buffer);

        int ChannelCount(){ return info_.channels; }
        int SampleRate(){ return info_.samplerate; }
        int Size(){ return wave_size_; }
        int ChannelLength() { return channel_length_; }

        WaveFileChannel<value_type>& operator[](int index);

    private:
        int wave_size_;
        int channel_length_;
        SF_INFO info_;
        WaveFileChannel<value_type> **channels_;

        void split_channels(value_type* file_buffer);
};

template<typename value_type>
WaveFile<value_type>::WaveFile(SF_INFO info, int wave_size, value_type* file_buffer)
{
    wave_size_ = wave_size;   
    channel_length_ = wave_size / info.channels;
    info_ = info;

    channels_ = new WaveFileChannel<value_type>*[info.channels];
    split_channels(file_buffer);
}

template<typename value_type>
WaveFileChannel<value_type>& WaveFile<value_type>::operator[](int index)
{
    if(index >= info_.channels)
        exit(-1);
        // throw new std::out_of_range("out of bounds on wave file channels, max: " << std::to_string(info_.channels - 1) << "index used: " << std::to_string(index))
    else
        return *channels_[index];
}

template<typename value_type>
void WaveFile<value_type>::split_channels(value_type* file_buffer)
{
    int channel_length = wave_size_ / info_.channels;
    value_type **channels = new value_type*[info_.channels];

    for(int i = 0; i < info_.channels; i++)
        channels[i] = new value_type[channel_length];

    for(int i = 0; i < channel_length; i++)
        for(int j = 0; j < info_.channels; j++)
            channels[j][i] = file_buffer[info_.channels*i + j];

    for(int i = 0; i < info_.channels; i++)
        channels_[i] = new WaveFileChannel<value_type>(channels[i], channel_length);
}