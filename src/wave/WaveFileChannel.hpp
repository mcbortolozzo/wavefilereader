#pragma once

#include <stdexcept>
#include <cstring>

#include "WaveWindowIterator.hpp"

template<typename value_type>
class WaveFileChannel
{
    public:
        WaveFileChannel() {}
        WaveFileChannel(value_type* channel_buffer, int channel_length);
        value_type operator[](int index);
        WaveWindowIterator<value_type> operator[](std::pair<int, int> range);
        int Length() { return channel_length_; }

        inline value_type* Buffer() { return channel_buffer_; }

    private:
        value_type* channel_buffer_;
        int channel_length_;
};

template<typename value_type>
WaveFileChannel<value_type>::WaveFileChannel(value_type* channel_buffer, int channel_length)
{
    channel_buffer_ = channel_buffer;
    channel_length_ = channel_length;
}

template<typename value_type>
value_type WaveFileChannel<value_type>::operator[](int index)
{ 
    if(channel_length_ <= index || index < 0)
        exit(-1);
        // throw new std::out_of_range("out of bounds on channel buffer, max: " << channel_length_ - 1<< "index used: " << index);
    else
        return channel_buffer_[index];
}

template<typename value_type>
WaveWindowIterator<value_type> WaveFileChannel<value_type>::operator[](std::pair<int, int> range)
{ 
    //todo validate start < end
    //     end > 0
    //     start < buffer
    int read_start = range.first;
    int read_end = range.second;
    int before_padding = read_start < 0 ? -1*read_start : 0;
    int after_padding = read_end > channel_length_ ? read_end - channel_length_ : 0;

    return WaveWindowIterator<value_type>(channel_buffer_, channel_length_, read_start, before_padding, after_padding);
}