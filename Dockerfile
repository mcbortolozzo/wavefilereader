# GCC support can be specified at major, minor, or micro version
# (e.g. 8, 8.2 or 8.2.0).
# See https://hub.docker.com/r/library/gcc/ for all supported GCC
# tags from Docker Hub.
# See https://docs.docker.com/samples/library/gcc/ for more on how to use this image
FROM gcc:latest

RUN apt-get update && apt-get install 

RUN curl -O http://www.mega-nerd.com/libsndfile/files/libsndfile-1.0.28.tar.gz
RUN tar -xzvf libsndfile-1.0.28.tar.gz
WORKDIR ./libsndfile-1.0.28
RUN ./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/libsndfile-1.0.28 && make
RUN make install

RUN curl -O http://www.fftw.org/fftw-3.3.8.tar.gz
RUN tar -xzvf fftw-3.3.8.tar.gz
WORKDIR ./fftw-3.3.8
RUN ./configure
RUN make
RUN make install

# These commands copy your files into the specified directory in the image
# and set that as the working location
COPY . /usr/src/wavFileAnalyzer
WORKDIR /usr/src/wavFileAnalyzer

# This command compiles your app using GCC, adjust for your source code
# RUN g++ -I $HOME/local/include -L $HOME/local/lib -lsndfile -o tests/WavFileAnalyzerTest tests/WaveFileAnalyzerTest.cpp src/wave/WaveFileAnalyzer.cpp
RUN make test
# RUN g++ -I $HOME/local/include -L $HOME/local/lib -lsndfile -o WavFileAnalyzer WaveFileAnalyzer.cpp Main.cpp

# This command runs your application, comment out this line to compile only
CMD ["./wavefileopenertest"]

LABEL Name=wavfileopener Version=0.0.1